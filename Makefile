./bin/punteros: ./src/punteros.c
	mkdir -p ./bin/
	gcc -Wall -fsanitize=address,undefined ./src/punteros.c -o ./bin/punteros

.PHONY: clean
clean:
	rm bin/ -rf